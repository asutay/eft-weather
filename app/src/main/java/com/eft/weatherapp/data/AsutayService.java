package com.eft.weatherapp.data;

import com.eft.weatherapp.data.model.response.DailyWeatherResponse;
import com.eft.weatherapp.data.model.response.WeatherResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by ServetCanAsutay on 02/02/17.
 */

public interface AsutayService {

    @GET("premium/v1/weather.ashx?key=648613b489c748d887e135417171303&num_of_days=5&tp=1&format=json")
    Observable<WeatherResponse> getHourlyWeather(@Query("q") String city);

    @GET("premium/v1/weather.ashx?key=648613b489c748d887e135417171303&num_of_days=5&tp=24&format=json")
    Observable<DailyWeatherResponse> getDailyWeather(@Query("q") String city);
}

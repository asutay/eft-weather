package com.eft.weatherapp.data.model.common;

/**
 * Created by ServetCanAsutay on 14/03/17.
 */

public class WeatherDesc {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

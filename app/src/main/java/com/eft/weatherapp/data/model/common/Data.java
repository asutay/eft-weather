package com.eft.weatherapp.data.model.common;

import java.util.List;

/**
 * Created by ServetCanAsutay on 14/03/17.
 */

public class Data {
    private List<Request> request;
    private List<CurrentCondition> current_condition;
    private List<Weather> weather;
    private List<Error> error;

    public List<Request> getRequest() {
        return request;
    }

    public void setRequest(List<Request> request) {
        this.request = request;
    }

    public List<CurrentCondition> getCurrent_condition() {
        return current_condition;
    }

    public void setCurrent_condition(List<CurrentCondition> current_condition) {
        this.current_condition = current_condition;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public List<Error> getError() {
        return error;
    }

    public void setError(List<Error> error) {
        this.error = error;
    }
}

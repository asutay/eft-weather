package com.eft.weatherapp.data.model.response;

import com.eft.weatherapp.data.model.common.Data;

/**
 * Created by ServetCanAsutay on 14/03/17.
 */

public class WeatherResponse {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}

package com.eft.weatherapp.data.model.common;

/**
 * Created by ServetCanAsutay on 14/03/17.
 */

public class City {
    private String name;
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

package com.eft.weatherapp.data.model.common;

/**
 * Created by ServetCanAsutay on 14/03/17.
 */

public class Error {
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}

package com.eft.weatherapp.data.model.common;

/**
 * Created by ServetCanAsutay on 14/03/17.
 */

public class Request {
    private String type;
    private String query;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}

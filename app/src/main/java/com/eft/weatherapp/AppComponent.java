package com.eft.weatherapp;

import com.eft.weatherapp.data.DataModule;
import com.eft.weatherapp.interactors.InteractorsModule;
import com.eft.weatherapp.interactors.WeatherInteractor;
import com.eft.weatherapp.util.RxBus;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

@Singleton
@Component(
        modules = {
                AppModule.class,
                DataModule.class,
                InteractorsModule.class
        }
)

public interface AppComponent {
    void inject(App app);

    WeatherInteractor getWeatherInteractor();

    RxBus getRxBus();
    Picasso getPicasso();

}

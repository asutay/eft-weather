package com.eft.weatherapp.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public class DateUtils {
    public static final String format = "yyyy.MM.dd hh:mm:ss";
    public static final String formatRequest = "yyyyMMddHHmm";

    public static String convertToServerDate(Date time) {
        SimpleDateFormat formatter = new SimpleDateFormat(DateUtils.formatRequest);
        TimeZone timezone = TimeZone.getTimeZone("UTC");
        formatter.setTimeZone(timezone);
        return formatter.format(time);
    }

    public static String convertHour(String hour) {
        String formattedHour = "";

        if (hour.length() == 0)
            hour = "0000";

        if (hour.length() == 1)
            hour = "000" + hour;

        if (hour.length() == 2)
            hour = "00" + hour;

        if (hour.length() == 3)
            hour = "0" + hour;

        formattedHour = hour.substring(0, 2) + ":" + hour.substring(2, hour.length());

        return formattedHour;
    }

    public static String getDay(String strDate) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dddd-MM-yyyy");
        String day = "";

        try {
            Date date = inputFormat.parse(strDate);
            String outputDateStr = outputFormat.format(date);
            Date outputDate = outputFormat.parse(outputDateStr);

            day = android.text.format.DateFormat.format("EEEE", outputDate).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return day;
    }

    public static String  getFormattedDate(String strDate) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd.MM.yyyy");
        String formattedDate = "";

        try {
            Date date = inputFormat.parse(strDate);
            formattedDate = outputFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formattedDate;
    }
}

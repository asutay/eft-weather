package com.eft.weatherapp.util;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.eft.weatherapp.ui.cities.adapter.CityListAdapter;

/**
 * Created by ServetCanAsutay on 14/03/17.
 */

public class SwipeHelper extends ItemTouchHelper.SimpleCallback {

    CityListAdapter adapter;

    public SwipeHelper(int dragDirs, int swipeDirs) {
        super(dragDirs, swipeDirs);
    }

    public SwipeHelper(CityListAdapter adapter) {
        super(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT);
        this.adapter = adapter;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
       adapter.dismiss(viewHolder.getAdapterPosition());
    }
}

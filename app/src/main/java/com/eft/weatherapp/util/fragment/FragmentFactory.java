package com.eft.weatherapp.util.fragment;

import com.eft.weatherapp.ui.cities.CityFragment;
import com.eft.weatherapp.ui.home.HomeFragment;
import com.eft.weatherapp.ui.splash.SplashFragment;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public class FragmentFactory {

    private static FragmentFactory instance;

    private FragmentFactory() {
    }

    public static FragmentFactory getInstance() {
        if (instance == null)
            instance = new FragmentFactory();

        return instance;
    }

    public FragmentBuilder getFragment(boolean isTablet, Page page, Object... obj) {
        FragmentBuilder fragmentBuilder = null;

        switch (page) {
            case SPLASH:
                fragmentBuilder = new FragmentBuilder().setFragment(SplashFragment.newInstance()).setTransactionAnimation(TransactionAnimation.NO_ANIM);
                break;

            case HOME:
                fragmentBuilder = new FragmentBuilder();
                fragmentBuilder.setClearBackStack(true);
                fragmentBuilder.setFragment(HomeFragment.newInstance());
                break;

            case CITY_PAGE:
                fragmentBuilder = new FragmentBuilder().setFragment(CityFragment.newInstance()).setAddToBackStack(true);
                break;
        }

        return fragmentBuilder;
    }
}

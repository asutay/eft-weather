package com.eft.weatherapp.util.fragment;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public enum FragmentTransactionType {
    REPLACE, ADD
}

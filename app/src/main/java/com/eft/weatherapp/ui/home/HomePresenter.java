package com.eft.weatherapp.ui.home;

import android.content.Context;

import com.eft.weatherapp.base.BaseFragmentPresenter;
import com.eft.weatherapp.util.RxBus;

import javax.inject.Inject;

public class HomePresenter extends BaseFragmentPresenter<HomeView> {

    Context context;

    @Override
    public void onResponse(Object response) {

    }

    public void onFragmentStart(Context context) {
        this.context = context;
    }

    @Inject
    public HomePresenter(RxBus rxBus) {
        super(rxBus);
    }

}

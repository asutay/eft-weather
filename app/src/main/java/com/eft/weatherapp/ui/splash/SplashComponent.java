package com.eft.weatherapp.ui.splash;

import com.eft.weatherapp.AppComponent;
import com.eft.weatherapp.base.BaseModule;
import com.eft.weatherapp.base.scopes.FragmentScope;

import dagger.Component;

@FragmentScope
@Component(
        dependencies = AppComponent.class,
        modules = BaseModule.class
)
public interface SplashComponent {
    void inject(SplashFragment fragment);

    SplashPresenter getSplashPresenter();
}

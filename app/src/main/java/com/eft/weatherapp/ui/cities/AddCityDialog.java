package com.eft.weatherapp.ui.cities;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.eft.weatherapp.R;

/**
 * Created by ServetCanAsutay on 14/03/17.
 */

public class AddCityDialog extends Dialog {

    public AddCityDialog(Context context) {
        super(context);

        setCanceledOnTouchOutside(false);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_add_city);
        this.getWindow().setBackgroundDrawable(null);
        this.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        ImageView ivClose = (ImageView) findViewById(R.id.dialog_add_city_close_iv);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }
}
package com.eft.weatherapp.ui.home;

import com.eft.weatherapp.AppComponent;
import com.eft.weatherapp.base.BaseModule;
import com.eft.weatherapp.base.scopes.FragmentScope;

import dagger.Component;

@FragmentScope
@Component(
        dependencies = AppComponent.class,
        modules = BaseModule.class
)
public interface HomeComponent {
    void inject(HomeFragment fragment);

    HomePresenter getHomePresenter();
}

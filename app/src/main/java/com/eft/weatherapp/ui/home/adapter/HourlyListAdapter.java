package com.eft.weatherapp.ui.home.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eft.weatherapp.R;
import com.eft.weatherapp.data.model.common.Hourly;
import com.eft.weatherapp.util.DateUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ServetCanAsutay on 14/03/17.
 */

public class HourlyListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Hourly> hourly;

    public HourlyListAdapter(Context context, List<Hourly> hourly) {
        this.context = context;
        this.hourly = hourly;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hourly, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;

        Hourly hourItem = new Hourly();

        if (hourly != null && hourly.get(position) != null)
            hourItem = hourly.get(position);

        if (hourItem.getTime() != null)
            viewHolder.tvHour.setText(DateUtils.convertHour(hourItem.getTime()));

        if (hourItem.getTempC() != null)
            viewHolder.tvDegree.setText(hourItem.getTempC() + "\u00B0");

        if (hourItem.getWeatherIconUrl() != null && hourItem.getWeatherIconUrl().size() > 0 && hourItem.getWeatherIconUrl().get(0) != null && hourItem.getWeatherIconUrl().get(0).getValue() != null)
            Picasso.with(context).load(hourItem.getWeatherIconUrl().get(0).getValue()).into(viewHolder.imageView);

    }

    @Override
    public int getItemCount() {
        return hourly.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_hourly_hour_tv)
        TextView tvHour;

        @BindView(R.id.item_hourly_iv)
        ImageView imageView;

        @BindView(R.id.item_hourly_degree_tv)
        TextView tvDegree;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}

package com.eft.weatherapp.ui.cities;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.eft.weatherapp.AppComponent;
import com.eft.weatherapp.R;
import com.eft.weatherapp.ResponseSingleton;
import com.eft.weatherapp.base.BaseFragment;
import com.eft.weatherapp.data.model.common.City;
import com.eft.weatherapp.ui.cities.adapter.CityListAdapter;
import com.eft.weatherapp.util.SwipeHelper;
import com.eft.weatherapp.util.fragment.Page;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public final class CityFragment extends BaseFragment<CityView, CityPresenter> implements CityView, CityListAdapter.CityClickListener {

    private CityComponent component;

    CityListAdapter adapter;

    private List<City> cities;

    @BindView(R.id.fragment_city_recycler_view)
    RecyclerView recyclerView;

    public static CityFragment newInstance() {
        return new CityFragment();
    }

    @Override
    public void onFragmentStarted() {
        getPresenter().onFragmentStart(getContext());

        cities = ResponseSingleton.getInstance().getCities();

        setViews();
    }

    @Override
    public Page getPage() {
        return Page.CITY_PAGE;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_city;
    }


    @Override
    protected void injectDependencies(@NonNull AppComponent parentComponent) {
        component = DaggerCityComponent.builder()
                .appComponent(parentComponent)
                .build();
        component.inject(this);
    }

    @Override
    public CityPresenter createPresenter() {
        return component.getCityPresenter();
    }

    private void setViews() {
        adapter = new CityListAdapter(getContext(), cities);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter.setCityClickListener(this);
        recyclerView.setAdapter(adapter);

        ItemTouchHelper.Callback callback = new SwipeHelper(adapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(recyclerView);
    }

    @OnClick(R.id.fragment_city_back_iv)
    public void backClicked() {
        onBackPress();
    }

    @OnClick(R.id.fragment_city_plus_iv)
    public void addClicked() {
        final AddCityDialog dialog = new AddCityDialog(getContext());
        dialog.show();

        LinearLayout llConfirm = (LinearLayout) dialog.findViewById(R.id.dialog_add_city_confirm_button);
        final EditText editText = (EditText) dialog.findViewById(R.id.dialog_add_city_et);

        llConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (!TextUtils.isEmpty(editText.getText().toString()))
                    addCity(editText.getText().toString());
            }
        });
    }

    private void addCity(String cityStr) {
        City city = new City();
        city.setName(cityStr);

        cityStr = cityStr.replace(" ", "+");
        city.setCode(cityStr);
        adapter.add(city);
    }

    @Override
    public void onCityClicked(City city, int position) {
        getPresenter().getWeather(city.getCode());
    }

    @Override
    public void onCitySelected() {
        onBackPress();
    }
}

package com.eft.weatherapp.ui.cities;

import com.eft.weatherapp.AppComponent;
import com.eft.weatherapp.base.BaseModule;
import com.eft.weatherapp.base.scopes.FragmentScope;

import dagger.Component;

@FragmentScope
@Component(
        dependencies = AppComponent.class,
        modules = BaseModule.class
)
public interface CityComponent {
    void inject(CityFragment fragment);

    CityPresenter getCityPresenter();
}

package com.eft.weatherapp.ui.cities;

import com.eft.weatherapp.base.BaseFragmentView;

public interface CityView extends BaseFragmentView {
    void onCitySelected();
}

package com.eft.weatherapp.ui.main.controller;


import com.eft.weatherapp.base.BaseView;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

public interface MainView extends BaseView {
    void setFragmentAdapter();
}

package com.eft.weatherapp.ui.cities;

import android.content.Context;

import com.eft.weatherapp.ResponseSingleton;
import com.eft.weatherapp.base.BaseFragmentPresenter;
import com.eft.weatherapp.data.model.response.DailyWeatherResponse;
import com.eft.weatherapp.data.model.response.WeatherResponse;
import com.eft.weatherapp.interactors.WeatherInteractor;
import com.eft.weatherapp.util.RxBus;

import javax.inject.Inject;

public class CityPresenter extends BaseFragmentPresenter<CityView> {

    Context context;

    private String cityCode;

    @Inject
    WeatherInteractor weatherInteractor;

    @Override
    public void onResponse(Object response) {
        if (response instanceof WeatherResponse) {
            WeatherResponse weatherResponse = (WeatherResponse) response;

            ResponseSingleton.getInstance().setWeatherResponse(weatherResponse);
            getDailyWeather();
        } else if (response instanceof DailyWeatherResponse) {
            DailyWeatherResponse dailyWeatherResponse = (DailyWeatherResponse) response;

            ResponseSingleton.getInstance().setDailyWeatherResponse(dailyWeatherResponse);
            getView().unlockScreen();
            getView().onCitySelected();
        }
    }

    public void onFragmentStart(Context context) {
        this.context = context;
    }

    @Inject
    public CityPresenter(RxBus rxBus) {
        super(rxBus);
    }

    public void getWeather(String cityCode) {
        getView().lockScreen();
        this.cityCode = cityCode;
        weatherInteractor.getHourlyWeather(cityCode);
    }

    private void getDailyWeather() {
        weatherInteractor.getDailyWeather(cityCode);
    }

}

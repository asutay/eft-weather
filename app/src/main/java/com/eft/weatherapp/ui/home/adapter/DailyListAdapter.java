package com.eft.weatherapp.ui.home.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eft.weatherapp.R;
import com.eft.weatherapp.data.model.common.Weather;
import com.eft.weatherapp.util.DateUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ServetCanAsutay on 14/03/17.
 */

public class DailyListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Weather> weathers;

    public DailyListAdapter(Context context, List<Weather> weathers) {
        this.context = context;
        this.weathers = weathers;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_daily, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;

        Weather weather = new Weather();

        if (weathers != null && weathers.get(position) != null)
            weather = weathers.get(position);

        if (weather.getDate() != null) {
            viewHolder.tvDate.setText(DateUtils.getFormattedDate(weather.getDate()));
            viewHolder.tvDay.setText(DateUtils.getDay(weather.getDate()));
        }

        if (weather.getHourly() != null && weather.getHourly().size() > 0 && weather.getHourly().get(0) != null && weather.getHourly().get(0).getWeatherIconUrl() != null &&
                weather.getHourly().get(0).getWeatherIconUrl().size() > 0 && weather.getHourly().get(0).getWeatherIconUrl().get(0) != null && weather.getHourly().get(0).getWeatherIconUrl().get(0).getValue() != null)
            Picasso.with(context).load(weather.getHourly().get(0).getWeatherIconUrl().get(0).getValue()).into(viewHolder.imageView);

        if (weather.getMaxtempC() != null)
            viewHolder.tvMaxDegree.setText(weather.getMaxtempC() + "\u00B0");

        if (weather.getMintempC() != null)
            viewHolder.tvMinDegree.setText(weather.getMintempC() + "\u00B0");

    }

    @Override
    public int getItemCount() {
        return weathers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_daily_date_tv)
        TextView tvDate;

        @BindView(R.id.item_daily_day_tv)
        TextView tvDay;

        @BindView(R.id.item_daily_iv)
        ImageView imageView;

        @BindView(R.id.item_daily_max_degree_tv)
        TextView tvMaxDegree;

        @BindView(R.id.item_daily_min_degree_tv)
        TextView tvMinDegree;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
package com.eft.weatherapp.ui.home;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.eft.weatherapp.AppComponent;
import com.eft.weatherapp.R;
import com.eft.weatherapp.ResponseSingleton;
import com.eft.weatherapp.base.BaseFragment;
import com.eft.weatherapp.data.model.common.CurrentCondition;
import com.eft.weatherapp.data.model.response.DailyWeatherResponse;
import com.eft.weatherapp.data.model.response.WeatherResponse;
import com.eft.weatherapp.ui.home.adapter.DailyListAdapter;
import com.eft.weatherapp.ui.home.adapter.HourlyListAdapter;
import com.eft.weatherapp.util.fragment.Page;

import butterknife.BindView;
import butterknife.OnClick;

public final class HomeFragment extends BaseFragment<HomeView, HomePresenter> implements HomeView {

    private HomeComponent component;

    @BindView(R.id.fragment_home_city_tv)
    TextView tvCity;

    @BindView(R.id.fragment_home_status_tv)
    TextView tvStatus;

    @BindView(R.id.fragment_home_degree_tv)
    TextView tvDegree;

    @BindView(R.id.fragment_home_hourly_recycler_view)
    RecyclerView rvHourly;

    @BindView(R.id.fragment_home_daily_recycler_view)
    RecyclerView rvDaily;

    private WeatherResponse weatherResponse;
    private DailyWeatherResponse dailyWeatherResponse;


    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onFragmentStarted() {
        getPresenter().onFragmentStart(getContext());

        if (ResponseSingleton.getInstance() != null && ResponseSingleton.getInstance().getWeatherResponse() != null)
            weatherResponse = ResponseSingleton.getInstance().getWeatherResponse();

        if (ResponseSingleton.getInstance() != null && ResponseSingleton.getInstance().getDailyWeatherResponse() != null)
            dailyWeatherResponse = ResponseSingleton.getInstance().getDailyWeatherResponse();

        fillViews();
    }

    @Override
    public Page getPage() {
        return Page.HOME;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }


    @Override
    protected void injectDependencies(@NonNull AppComponent parentComponent) {
        component = DaggerHomeComponent.builder()
                .appComponent(parentComponent)
                .build();
        component.inject(this);
    }

    @Override
    public HomePresenter createPresenter() {
        return component.getHomePresenter();
    }

    private void fillViews() {

        if (weatherResponse != null && weatherResponse.getData() != null) {

            if (weatherResponse.getData().getRequest() != null && weatherResponse.getData().getRequest().size() > 0 && weatherResponse.getData().getRequest().get(0) != null && weatherResponse.getData().getRequest().get(0).getQuery() != null)
                tvCity.setText(weatherResponse.getData().getRequest().get(0).getQuery());

            if (weatherResponse.getData().getCurrent_condition() != null && weatherResponse.getData().getCurrent_condition().size() > 0 && weatherResponse.getData().getCurrent_condition().get(0) != null) {
                CurrentCondition currentCondition = weatherResponse.getData().getCurrent_condition().get(0);

                if (currentCondition.getWeatherDesc() != null && currentCondition.getWeatherDesc().size() > 0 && currentCondition.getWeatherDesc().get(0) != null && currentCondition.getWeatherDesc().get(0).getValue() != null)
                    tvStatus.setText(currentCondition.getWeatherDesc().get(0).getValue());

                if (currentCondition.getTemp_C() != null)
                    tvDegree.setText(currentCondition.getTemp_C() + "\u00B0");
            }

            if (weatherResponse.getData().getWeather() != null && weatherResponse.getData().getWeather().size() > 0 && weatherResponse.getData().getWeather().get(0) != null && weatherResponse.getData().getWeather().get(0).getHourly() != null)
                setHourlyList();
        }

        if (dailyWeatherResponse != null && dailyWeatherResponse.getData() != null && dailyWeatherResponse.getData().getWeather() != null)
            setDailyList();
    }

    private void setHourlyList() {
        HourlyListAdapter adapter = new HourlyListAdapter(getContext(), weatherResponse.getData().getWeather().get(0).getHourly());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvHourly.setLayoutManager(layoutManager);
        rvHourly.setAdapter(adapter);
    }

    private void setDailyList() {
        DailyListAdapter adapter = new DailyListAdapter(getContext(), dailyWeatherResponse.getData().getWeather());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvDaily.setLayoutManager(layoutManager);
        rvDaily.setAdapter(adapter);
    }

    @OnClick(R.id.fragment_home_menu_iv)
    public void menuClicked() {
        showPage(Page.CITY_PAGE);
    }

}

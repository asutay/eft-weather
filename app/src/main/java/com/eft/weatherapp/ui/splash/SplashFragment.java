package com.eft.weatherapp.ui.splash;

import android.support.annotation.NonNull;

import com.eft.weatherapp.AppComponent;
import com.eft.weatherapp.R;
import com.eft.weatherapp.base.BaseFragment;
import com.eft.weatherapp.util.fragment.Page;

public final class SplashFragment extends BaseFragment<SplashView, SplashPresenter> implements SplashView {

    private SplashComponent component;

    public static SplashFragment newInstance() {
        return new SplashFragment();
    }

    @Override
    public void onFragmentStarted() {
        getPresenter().onFragmentStart(getContext());
    }

    @Override
    public Page getPage() {
        return Page.SPLASH;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_splash;
    }


    @Override
    protected void injectDependencies(@NonNull AppComponent parentComponent) {
        component = DaggerSplashComponent.builder()
                .appComponent(parentComponent)
                .build();
        component.inject(this);
    }

    @Override
    public SplashPresenter createPresenter() {
        return component.getSplashPresenter();
    }

}

package com.eft.weatherapp.ui.main.controller;

import com.eft.weatherapp.AppComponent;
import com.eft.weatherapp.base.BaseModule;
import com.eft.weatherapp.base.scopes.ActivityScope;

import dagger.Component;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = BaseModule.class
)
public interface MainComponent {
    void inject(MainActivity mainActivity);
    MainPresenter getMainPresenter();
}

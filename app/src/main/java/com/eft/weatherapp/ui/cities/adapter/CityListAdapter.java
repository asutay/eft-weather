package com.eft.weatherapp.ui.cities.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eft.weatherapp.R;
import com.eft.weatherapp.data.model.common.City;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ServetCanAsutay on 14/03/17.
 */

public class CityListAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<City> cities;

    private CityClickListener cityClickListener;

    public CityListAdapter(Context context, List<City> cities) {
        this.context = context;
        this.cities = cities;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ViewHolder viewHolder = (ViewHolder) holder;

        viewHolder.tvCity.setText(cities.get(position).getName());

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cityClickListener.onCityClicked(cities.get(position), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_city_name_tv)
        TextView tvCity;

        @BindView(R.id.item_city_container)
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void dismiss(int position) {
        cities.remove(position);
        this.notifyItemRemoved(position);
        this.notifyItemRangeChanged(position, cities.size());
    }

    public void add(City city) {
        cities.add(city);
        this.notifyItemInserted(cities.size() - 1);
        this.notifyItemRangeChanged(cities.size() - 1, cities.size());
    }

    public interface CityClickListener {
        void onCityClicked(City city, int position);
    }

    public void setCityClickListener(CityClickListener cityClickListener) {
        this.cityClickListener = cityClickListener;
    }

}
package com.eft.weatherapp.ui.splash;

import android.content.Context;

import com.eft.weatherapp.ResponseSingleton;
import com.eft.weatherapp.base.BaseFragmentPresenter;
import com.eft.weatherapp.data.model.common.City;
import com.eft.weatherapp.data.model.response.DailyWeatherResponse;
import com.eft.weatherapp.data.model.response.WeatherResponse;
import com.eft.weatherapp.interactors.WeatherInteractor;
import com.eft.weatherapp.util.RxBus;
import com.eft.weatherapp.util.fragment.Page;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SplashPresenter extends BaseFragmentPresenter<SplashView> {

    Context context;

    @Inject
    WeatherInteractor weatherInteractor;

    @Override
    public void onResponse(Object response) {
        if (response instanceof WeatherResponse) {
            WeatherResponse weatherResponse = (WeatherResponse) response;

            ResponseSingleton.getInstance().setWeatherResponse(weatherResponse);
            getDailyWeather();
        } else if (response instanceof DailyWeatherResponse) {
            DailyWeatherResponse dailyWeatherResponse = (DailyWeatherResponse) response;

            ResponseSingleton.getInstance().setDailyWeatherResponse(dailyWeatherResponse);
            getView().showPage(Page.HOME);
        }
    }

    public void onFragmentStart(Context context) {
        this.context = context;
        generateInitialCities();
        getWeather();
    }

    @Inject
    public SplashPresenter(RxBus rxBus) {
        super(rxBus);
    }

    private void generateInitialCities() {
        List<City> cities = new ArrayList<>();

        City city1 = new City();
        city1.setCode("london");
        city1.setName("London");

        City city2 = new City();
        city2.setCode("valletta");
        city2.setName("Valletta");

        City city3 = new City();
        city3.setCode("barcelona");
        city3.setName("Barcelona");

        City city4 = new City();
        city4.setCode("istanbul");
        city4.setName("Istanbul");

        City city5 = new City();
        city5.setCode("new+york");
        city5.setName("New York");

        cities.add(city1);
        cities.add(city2);
        cities.add(city3);
        cities.add(city4);
        cities.add(city5);

        ResponseSingleton.getInstance().setCities(cities);
    }

    private void getWeather() {
        weatherInteractor.getHourlyWeather("London");
    }

    private void getDailyWeather() {
        weatherInteractor.getDailyWeather("London");
    }

}

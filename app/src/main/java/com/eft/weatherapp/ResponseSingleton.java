package com.eft.weatherapp;

import com.eft.weatherapp.data.model.common.City;
import com.eft.weatherapp.data.model.response.DailyWeatherResponse;
import com.eft.weatherapp.data.model.response.WeatherResponse;

import java.util.List;

/**
 * Created by ServetCanAsutay on 07/12/16.
 */

public class ResponseSingleton {
    private static ResponseSingleton ourInstance = new ResponseSingleton();

    public static ResponseSingleton getInstance() {
        return ourInstance;
    }

    private ResponseSingleton() {
    }

    private WeatherResponse weatherResponse;
    private DailyWeatherResponse dailyWeatherResponse;

    private List<City> cities;

    public WeatherResponse getWeatherResponse() {
        return weatherResponse;
    }

    public void setWeatherResponse(WeatherResponse weatherResponse) {
        this.weatherResponse = weatherResponse;
    }

    public DailyWeatherResponse getDailyWeatherResponse() {
        return dailyWeatherResponse;
    }

    public void setDailyWeatherResponse(DailyWeatherResponse dailyWeatherResponse) {
        this.dailyWeatherResponse = dailyWeatherResponse;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
}

package com.eft.weatherapp.interactors;

import com.eft.weatherapp.data.AsutayService;
import com.eft.weatherapp.util.RxBus;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ServetCanAsutay on 01/12/16.
 */

// TODO: 01/12/16 Düzenle
@Module
public class InteractorsModule {

    @Provides
    public WeatherInteractor provideWeatherInteractor(AsutayService asutayService , RxBus rxBus) {
        return new WeatherInteractor(asutayService, rxBus);
    }
}

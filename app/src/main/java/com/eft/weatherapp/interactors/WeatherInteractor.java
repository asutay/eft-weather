package com.eft.weatherapp.interactors;

import com.eft.weatherapp.base.ErrorEvent;
import com.eft.weatherapp.data.AsutayService;
import com.eft.weatherapp.data.model.response.DailyWeatherResponse;
import com.eft.weatherapp.data.model.response.WeatherResponse;
import com.eft.weatherapp.util.RxBus;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ServetCanAsutay on 14/03/17.
 */

public class WeatherInteractor extends BaseInteractor {

    public WeatherInteractor(AsutayService asutayService, RxBus rxBus) {
        super(asutayService, rxBus);
    }

    public void getHourlyWeather(String city) {
        getAsutayService().getHourlyWeather(city)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<WeatherResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getRxBus().send(new ErrorEvent(""));
                    }

                    @Override
                    public void onNext(WeatherResponse weatherResponse) {
                        if (weatherResponse != null && weatherResponse.getData() != null) {
                            if (weatherResponse.getData().getError() != null ) {
                                if (weatherResponse.getData().getError().size() > 0 && weatherResponse.getData().getError().get(0) != null && weatherResponse.getData().getError().get(0).getMsg() != null)
                                    getRxBus().send(new ErrorEvent(weatherResponse.getData().getError().get(0).getMsg()));
                                else
                                    getRxBus().send(new ErrorEvent(""));
                            } else {
                                getRxBus().send(weatherResponse);
                            }
                        } else {
                            getRxBus().send(new ErrorEvent(""));
                        }
                    }
                });
    }

    public void getDailyWeather(String city) {
        getAsutayService().getDailyWeather(city)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<DailyWeatherResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getRxBus().send(new ErrorEvent(""));
                    }

                    @Override
                    public void onNext(DailyWeatherResponse dailyWeatherResponse) {
                        if (dailyWeatherResponse != null && dailyWeatherResponse.getData() != null) {
                            if (dailyWeatherResponse.getData().getError() != null ) {
                                if (dailyWeatherResponse.getData().getError().size() > 0 && dailyWeatherResponse.getData().getError().get(0) != null && dailyWeatherResponse.getData().getError().get(0).getMsg() != null)
                                    getRxBus().send(new ErrorEvent(dailyWeatherResponse.getData().getError().get(0).getMsg()));
                                else
                                    getRxBus().send(new ErrorEvent(""));
                            } else {
                                getRxBus().send(dailyWeatherResponse);
                            }
                        } else {
                            getRxBus().send(new ErrorEvent(""));
                        }
                    }
                });
    }
}
